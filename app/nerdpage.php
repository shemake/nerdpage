<?php

/***** POSTS *****/

//returns a number of filenames from the posts directory
function getCSVPosts($dir = 'posts', $num = -1 )
{
    $files = array();
    $filename = $dir . "/posts.csv";

    if( !file_exists( $filename ) )
    {
        $files = FALSE;
    }
    else {

        //Map rows to array keys (in case format of posts.csv changes)
        $rows   = array_map('str_getcsv', file($filename, FILE_SKIP_EMPTY_LINES));
        $header = array_shift($rows);
        $csv    = array();
        $numRead = 0;

        foreach($rows as $row) {

            //Skip extra posts if $num is defined
            if( $num > 0 && $numRead >= $num )
            {
                break;
            }

            $line = array_combine($header, $row);
            if( file_exists($dir . "/" . $line["filename"]) )
            {
                $files[] = $line;
                $numRead++;
            }
        }
    }

    return $files;
}

//used by view.php to do lookup by post name
function getPost($post_nm, $dir = 'posts')
{
    $post_data = array();
    $filename = $dir . "/posts.csv";

    if( !file_exists( $filename ) )
    {
        $post_data = FALSE;
    }

    else {

        //Map rows to array keys (in case format of posts.csv changes)
        $rows   = array_map('str_getcsv', file($filename, FILE_SKIP_EMPTY_LINES));
        $header = array_shift($rows);
        $csv    = array();
        $numRead = 0;

        foreach($rows as $row) {
            $line = array_combine($header, $row);
            if( (file_ext_strip($line["filename"]) == $post_nm) &&
                file_exists($dir . "/" . $line["filename"]) )
            {
                $post_data = $line;
                break;
            }
        }
    }

    return $post_data;
}

//used by view to do lookup by post's numerical id
function getPostByID($post_id, $dir = 'posts')
{
    $post_data = array();
    $filename = $dir . "/posts.csv";

    if( !file_exists( $filename ) )
    {
        $post_data = FALSE;
    }

    else {

        //Map rows to array keys (in case format of posts.csv changes)
        $rows   = array_map('str_getcsv', file($filename, FILE_SKIP_EMPTY_LINES));
        $header = array_shift($rows);
        $csv    = array();
        $numRead = 0;

        foreach($rows as $row) {
            $line = array_combine($header, $row);
            if( ((int)$line["id"] == $post_id) &&
                file_exists($dir . "/" . $line["filename"]) )
            {
                $post_data = $line;
                break;
            }
        }
    }
    return $post_data;
}

//20220912 -> 09-12-2022
function formatLinuxDate($date)
{
    $yr = substr($date, 0, 4);
    $mt = substr($date, 4, 2);
    $dy = substr($date, 6, 2);
    $datestring = $mt . "-" . $dy . "-" . $yr;

    return $datestring;
}

//input: a line of csv from posts.csv
//output: a pretty link to the post
function getPostLink( $line )
{
    $html = "";
    if( !empty($line) )
    { 
        $filename = $line["filename"];
        $title = $line["title"];
        $html = "<a class=\"post_blurb\" href=\"./viewing/" . file_ext_strip($filename) . "\">" . $title . "</a>";
    }
    return $html;
}

// Returns the file name, less the extension.
function file_ext_strip($filename){
    return preg_replace('/.[^.]*$/', '', $filename);
}

// Increments the hit counter for this post in hits.csv
function recordHit($post_id, $dir, $addOne = true)
{
    $num_hits = -1;
    $post_id = (int)$post_id;

    $filename = $dir . "/hits.csv";
    $newfile = false;

    $fp = fopen($filename, 'r+'); //open for reading and writing
    if($fp === false)
    {
        $num_hits = -3;

        $fp = fopen($filename, 'w'); //try to open a new file in this location
        if( $fp === false )
        {
            $num_hits = -4;
            return $num_hits;
        }

        $newfile = true;
    }

    if(flock( $fp, LOCK_EX )) //there is now either a new file or an existing one at $fp
    {
        $lines = array();
        $found = false;
        if( !$newfile )
        {
            fgetcsv($fp); //skip first auto generated line.
            while(($data = fgetcsv($fp)) !== FALSE )
            {
                if( count($data) == 2 ) {
                    $pid = (int)$data[0];
                    if( $pid == $post_id ) {
                        $found = true;
                        $num_hits = (int)$data[1];
                        if( $addOne )
                        {
                            $num_hits++;
                        }
                        $data[1] = $num_hits;
                    }

                    $lines[] = $data;
                }
            }
        }

        // hits.csv is new, or we don't have a line for this post yet
        if( $newfile || !$found )
        {
            $num_hits = 1;
            $data = array($post_id, $num_hits);
            $lines[] = $data;
        }

        rewind($fp); //we've read the file, so put the pointer back at the beginning
        ftruncate($fp,0); //get rid of existing data, and rewrite the hits file

        //Now write lines back to the file
        $opening = "### THIS FILE IS AUTO GENERATED ###\n";
        fwrite($fp, $opening);
        foreach( $lines as $data )
        {
            fputcsv($fp, $data);
        }
    }
    else {
        $num_hits = -2;
    }

    fclose($fp); //this should release the file lock
    return $num_hits;
}

/***** COMMENTS *****/

function submitComment($comment_data, $post_id, $dir = "posts/comments")
{
    $max_author_len = $comment_data['max_author_len'];
    $max_subject_len = $comment_data['max_subject_len'];
    $max_email_len = $comment_data['max_email_len'];
    $max_comment_len = $comment_data['max_comment_len'];

    //Sanitize data
    $subject = trim(htmlentities($comment_data['subject']));
    $author = trim(htmlentities($comment_data['author']));
    $email = trim(htmlentities($comment_data['email']));
    $text = trim(htmlentities($comment_data['text']));
    $reply_to = trim(htmlentities($comment_data['reply_to']));

    if( isset($email) )
    {
        $email = filter_var($email, FILTER_VALIDATE_EMAIL);
        if(!$email)
        {
            $email = "malformed";            
        }
    }

    //Enforce string lengths
    if( strlen($subject) == 0 )
    {
        $subject = "(No Subject)";
    }
    else if( strlen($subject) > $max_subject_len )
    {
        $subject = substr($subject, 0, $max_subject_len) . '...';
    }

    if( strlen($author) == 0 )
    {
        $author = "Anonymous";
    }
    else if( strlen($author) > $max_author_len )
    {
        $author = substr($author, 0, $max_author_len) . '...';
    }

    if( strlen($email) == 0 )
    {
        $email = "";
    }
    else if( strlen($email) > $max_email_len )
    {
        $email = substr($email, 0, $max_email_len) . '...';
    }

    //Comment body is the only required field in this version
    if( strlen($text) == 0 )
    {
        return "Empty message, comment was not saved.";
    }
    else if( strlen($text) > $max_comment_len )
    {
        $text = substr($text, 0, $max_comment_len) . '...';
    }

    if( strlen($reply_to) == 0 )
    {
        $reply_to = "";
    }
    else
    {
        $matches = array();
        preg_match('/^p(\d+)c(\d+)$/', $reply_to, $matches);
        if( !count($matches) == 3 )
        {
            $reply_to = ""; //invalid reply to, not sure what to do with this.
        }
    }

    //The next line will edit (or create) comments.csv, and return the next
    // appropriate numerical id for this new comment, based off of the post
    // number 
    $comment_id = getLatestCommentID( $post_id, $dir );
    $msg = "";
    if( $comment_id < 0 )
    {
        $msg = "Error: can't get new comment id ( code " . $comment_id . ")";
    }
    else 
    {
        $datetime = gmdate("Y-m-d H:i:s", time());

        $cdata = array();
        $cdata['id'] = "p" . $post_id . "c" . $comment_id;
        $cdata['date'] = $datetime;
        $cdata['subject'] = $subject;
        $cdata['author'] = $author;
        $cdata['email'] = $email;
        $cdata['reply_to'] = $reply_to;
        $cdata['approved'] = false;
        $cdata['content'] = $text;
        $msg = writeCommentJSON( $cdata, $dir );

        //comment this out if you don't want to receive email notification of new comments
        notifyNewComment($cdata, $dir);
    }

    return $msg;
}

// Sends an email to the site owner with the location of the new comment .json file
function notifyNewComment($cdata, $dir)
{
    $ini_file = __DIR__ . "/np.ini";
    $ini = parse_ini_file($ini_file);
    $email = $ini["app_email"];
    $subject = "New comment posted to " . $ini["app_url"];
    $message = "There is a new comment waiting approval in " . $dir 
        . "\r\n" . "id = " . $cdata['id']
        . "\r\n" . "author = " . $cdata['author'];

    $headers = 'From: no-reply@shemake.dev' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    mail($email, $subject, $message, $headers );
}

// Creates a comment file
function writeCommentJSON($comment_data, $dir = "posts/comments")
{
    $json = json_encode($comment_data, JSON_PRETTY_PRINT);
    $comment_id = $comment_data["id"];
    $filename = $dir . "/" . $comment_id . ".json";

    if( !$fp = fopen($filename, 'w') )
    {
        return "Could not open comment file for writing. (". $filename . ")";
    }

    // Write some content to our opened file.
    /*
    //Sample comment file, named with post id and comment id
    p7c12.json
    {
        "id": "p7c12",
        "author": "John Doe",
        "email": "",
        "subject": "What is this?",
        "reply_to": "p7c11",
        "approved": true,
        "content": "Hey, it's a comment"
    }
     */
    if (fwrite($fp, $json) === FALSE) {
        return "Cannot write to file ($filename)";
    }

    fclose($fp);

    return "Comment submitted. Please wait for moderator review.";
}

function getLatestCommentID( $post_id, $dir = 'posts/comments' )
{
/*
comments.csv
post_id,last_comment_id
1,10
2,0
3,20

comments.csv is autogenerated, so that the person running the blog doesn't have to worry about it. 
When adding a comment:
* Scan the comments file to see if there is a line for the current post. 
* If so, read value, increment and write. Lock the file when opening it.
* If not, add a line to comments.csv:
  * Scan comments directory for comments on the current post. Check their reply_to sections as well. Find the highest comment ID and use that to make a new line.
 */

    //PHP will attempt to create the comments directory, but
    // the site owner may have to do this part manually, and make sure
    // that the permissions are set so that PHP can write to it
    // (see README.md)
    if (!file_exists($dir)) {
        mkdir($dir, 0766, true);
    }

    $comment_id = -1;
    $post_id = (int)$post_id;

    $filename = $dir . "/comments.csv";
    $newfile = false;

    $fp = fopen($filename, 'r+'); //open for reading and writing
    if($fp === false)
    {
        $comment_id = -3;

        $fp = fopen($filename, 'w'); //try to open a new file for writing in this location
        if( $fp === false )
        {
            $comment_id = -4;
            return $comment_id;
        }

        $newfile = true;
    }

    if(flock( $fp, LOCK_EX )) //there is now either a new file or an existing one at $fp
    {
        $lines = array();

        if( !$newfile )
        {
            fgetcsv($fp); //skip first auto generated line.
            while(($data = fgetcsv($fp)) !== FALSE )
            {
                if( count($data) == 2 ) {
                    $pid = (int)$data[0];
                    if( $pid == $post_id ) {
                        $comment_id = (int)$data[1] + 1;
                        $data[1] = $comment_id;
                    }

                    $lines[] = $data;
                }
            }
        }

        //Was not able to find a comment id for this post, so it's new
        //We must now find the highest comment id for this post, to ensure safety of reply_to, 
        // so search files beginning with p<post_id> and get the comment id
        if( $comment_id < 0 )
        {
            //get all json files in the comments directory starting with p<post_id>
            $postfiles = glob($dir . "/p" . $post_id . "*.json");
            $highest_id = 0;
            foreach( $postfiles as $file )
            {
                preg_match('/^p(\d+)c(\d+)/', $file, $matches);
                if( count($matches) == 3 )
                {
                    $found_comment_id = (int)$matches[2];
                    if($highest_id < $found_comment_id)
                    {
                        $highest_id = $found_comment_id;
                    }
                }

                $highest_id++;
            }

            $comment_id = $highest_id;
            $data = array($post_id, $comment_id);
            $lines[] = $data;
        }

        rewind($fp); //we've read the file, so put the pointer back at the beginning
        ftruncate($fp,0); //get rid of existing data, and rewrite the comments file

        //Now write lines back to the file
        $opening = "### THIS FILE IS AUTO GENERATED ###\n";
        fwrite($fp, $opening);
        foreach( $lines as $data )
        {
            fputcsv($fp, $data);
        }
    }
    else {
        $comment_id = -2;
    }

    fclose($fp); //this should release the file lock
    return $comment_id;
}

// Change this if you want a different date format
function getDisplayDate($timestamp)
{
    $date = strtotime($timestamp);
    return date("M d, Y", $date);
}

// Pretty link for the comment
function getCommentReplyLink($comment_id)
{
    $link = '<a href="../reply_to/' . $comment_id . '"\>Reply</a>';
    return $link;
}

// Turns a comment filename (e.g. p7c10) to an array with 
// post_id = 7 and comment_id = 10 
function parseCommentString( $comment_id )
{
    $data = false;
    preg_match('/^p(\d+)c(\d+)$/', $comment_id, $matches);
    if( count($matches) == 3 )
    {
        $data = array();
        $data['post_id'] = (int)$matches[1];
        $data['comment_id'] = (int)$matches[2];
    }
    return $data;
}

/* From a list of files, return a data set that looks like:
 * [p7c0] = { p7c0.json, {} } 
 * [p7c1] = { p7c1.json, {} }
 * [p7c2] = { p7c2.json, {p7c3.json, p7c4.json}}
 *
 * This shows that p7c3 and p7c4 are replies to p7c2.
 */
function groupComments($postfiles)
{
    $top_level = array();
    foreach($postfiles as $file)
    {
        $contents = file_get_contents($file);
        if( $contents )
        {
            $data = json_decode($contents, true);
            if( $data['reply_to'] === "" )
            {
                if( $data['approved'] )
                {
                    $top_level[$data['id']] = array("file" => $file, "subs" => array());
                }
            }
            else
            {
                foreach($top_level as $id => $files)
                {
                    if($id == $data['reply_to'])
                    {
                        if( $data['approved'] )
                        {
                            $top_level[$id]["subs"][] = $file;
                        }
                        break;
                    }
                }
            }
        }
    }

    return $top_level; 
}

//passes back a line from spamguard.csv (id, question )
function getSpamGuard()
{
    $question = array(); //return value
    $lines = array();
    $spamfile = __DIR__ . '/spamguard.csv';
    $fp = fopen($spamfile, 'r'); //open for reading
    if( $fp !== false )
    {
        while(($data = fgetcsv($fp)) !== FALSE )
        {
            if( count($data) == 3 ) {
                $line = array();
                $line['id'] = (int)$data[0];
                $line['question'] = $data[1];
                $lines[] = $line;
            }
        }
    }
    fclose($fp);

    $idx = array_rand($lines); //we want a random line from spamguard.csv to be returned
    if( $idx !== NULL && $idx >= 0 && $idx < count($lines) )
    {
        $question = $lines[$idx];
    }
    return $question;
}

//from a tuple [id, user's answer], determine if it matches
// the answer in spamguard.csv
function checkSpamGuard($input)
{
    $is_correct = false;
    $id = (int)$input['id'];
    $user_answer = strtolower(trim($input['answer']));

    $spamfile = __DIR__ . '/spamguard.csv';
    $fp = fopen($spamfile, 'r');
    if( $fp !== false )
    {
        while(($data = fgetcsv($fp)) !== FALSE )
        {
            if( count($data) == 3 ) {
                if( $data[0] == (int)$id )
                {
                    $file_answer = strtolower(trim($data[2]));
                    if( strcmp($user_answer, $file_answer) == 0 )
                    {
                        $is_correct = true;
                    }
                    break;
                }
            }
        }
    }
    fclose($fp);

    return $is_correct;
}

?>
