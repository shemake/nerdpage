
# NerdPage

## What is NerdPage?

NerdPage is a simple, file-based CMS. Posts are created as individual documents and are managed through CSV. No database installation is necessary, but a user should have basic familiarity with a command line or be able to edit config files on their webserver in some other way.

## Requirements

* PHP (min version **PHP 5.5.0**)
* sendmail (for notification of new comments only, not required otherwise)

## Typical NerdPage blog structure

```
> app/
> public_html/
>   .htaccess
>   index.php -- your site's main page. You can make it redirect to blog/ if you prefer
>   blog/
```

### blog folder

```
>
> - ./ 
> -   np-index.php
> -   np-view.php
> -   np-comments.php
> -   np-reply.php
> -   posts/
> -         posts.csv
> -         article1.html
> -         article2.html
> -         ...
> -         comments/
> -                 comments.csv
> -                 hits.csv
> -                 p0c0.json
> -                 p1c0.json
> -                 p1c1.json
> -                 ...
```

### app folder (one level above public_html):

```
> - ./
> -   nerdpage.php
> -   np.ini
> -   spamguard.csv
```

## Setting up NerdPage on your site

- Above **public_html**, create an **app** folder
- Into the **app** folder, place the following:
    - nerdpage.php
    - np.ini
    - spamguard.csv
- Edit **np.ini** with your domain and preferred email address
- Add or change spamguard questions by modifying **spamguard.csv**

## Setting up a new blog folder

1. Create a **blog** folder somewhere under **public_html**
2. Into that folder copy some files:
    - np-index.php
    - np-view.php
    - np-comments.php (if you want to enable comments)
    - np-reply.php (if you want to enable comments)
3. Within the **blog** folder, create a **posts** folder
4. In **blog/posts** create some files
    - posts.csv (a sample is included in the default installation)
    - a comments/ folder
5. Run the following commands to setup permissions correctly on the **comments/** folder for PHP:
    ```
    sudo chgrp -R www-data comments/
    sudo chmod g+rwxs comments/
    ```
6. Install and configure **sendmail** if you want to be notified when someone leaves a comment for approval
    ```
    sudo apt install sendmail
    sudo vim /etc/php/7.4/apache2/php.ini
    ```

## Adding a new article to your blog

1. Create a new document under **blog/posts/** (e.g. article1.html)
2. Add an entry to **blog/posts/posts.csv**

## Approving a comment for viewing on your site

By default, comments are not shown on your articles (this is a spam-prevention measure). When a user submits a comment, you should receive an email notification at the email specified in **app.ini** (providing that sendmail is set up correctly on your server). 

To approve a comment so that it will display on your site:
1. Navigate to **blog/posts/comments**
2. Open the appropriate comment JSON file. Comments are numbered by post and by comment number (e.g. p1c9.json)
3. Change the "approved" field from false to true

The comment will now display on your website.

## NOTES

### posts.csv (user-maintained)

The main engine of NerdPage. If you don't make a new entry for an article in this document, it won't show up by default in the list of articles.

### comments.csv (auto-generated)

This file just keeps track of the highest comment ID recorded for each post. It is auto-generated, so if the comments.csv is deleted, or if a line is deleted, it should self-repair. It's purpose is mostly to keep from having to query and count all of the files in the comments directory every time someone submits a new comment.

### hits.csv (auto-generated)

Page hits are recorded under blog/posts/comments/hits.csv. By default, blog/posts/comments is the ONLY folder that PHP is given access to, so that's where I put hits.csv, which needs to be updated every time a page is viewed.  You can see the view count for a particular article by default on view.php, but you can see all of the page counts in this file.

### spamguard.csv (user-maintained)

You can add or remove spamguard questions by editing this file. There are 10 default questions that come with NerdPage, but you can (and probably should) customize them occaisionally.

## Troubleshooting

The single most common error when setting up a NerdPage site is failing to create a comments directory, or failing to give that directory appropriate permissions for PHP to write to it. If the hits count at the top of one of your articles is -1, this is most likely the cause. To fix, change the owner and group of blog/posts/comments to www-data and make sure that it has write access. See setup for examples.

If you don't care about comments or page counts, you can simply un-include comments.php from blog/view.php and turn off the viewcount. This will make your site more secure than leaving it.

## Examples

I wrote NerdPage to manage my own personal site, [shemake.dev](https://shemake.dev), where I am running three separate blogs with this system (one copy of the app folder, three different "blog" folders). 

## Credits

Author: Willow Willis (@shemake)
