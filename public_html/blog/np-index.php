<?php
$path_to_ini = $_SERVER['DOCUMENT_ROOT'] . "/../app/np.ini";
$ini = parse_ini_file($path_to_ini);

require_once($ini["app_folder"] . "/nerdpage.php");

$blog_folder = "blog";
$latest_csv = getCSVPosts(); //you can enter a number to restrict this

//New posts are probably just being appended to the end of the CSV file, 
//so reverse the order for latest posts at top
array_multisort( array_column($latest_csv, "date"), SORT_DESC, $latest_csv);

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/blog.css">
<title><?php echo $blog_folder; ?></title>
</head>
<body>
<div class="main">
    <h1>NerdPage Example</h1>
    <p>Welcome to the main page of the blog. It can be self-contained under the rest of your site in its own folder:</p>
    <p>public_html/<br />
        * index.html <br />
        * blog/ <br />
        &nbsp;&nbsp;&nbsp;* index.php --- THIS PAGE <br />
    </p>
    <p>Running several blogs alongside each other is fine -- they will not interfere with each other. Please see the project's README.md for details on how to modify .htaccess for pretty links and other goodness.</p>
    <p>This page can be modified as needed to fit with any site layout and design.</p>
    <h2>Posts</h2> 
    <p>These are sorted with the most recent articles at the top, by default. That is easily changed by messing with this file.</p>
    <div id="latest_posts">
        <?php
            $cur_year = "0000";
            if( $latest_csv !== FALSE )
            {
                foreach($latest_csv as $line)
                {
                    $yr = substr($line["date"], 0, 4);

                    if($cur_year != $yr)
                    { 
                        if( $cur_year != "0000" )
                        {
                            echo "</ul>";
                        }
                        echo "<h3>" . $yr . "</h3>";
                        echo "<ul>";

                        $cur_year = $yr;
                    }
                    echo "<li>" . formatLinuxDate($line["date"]) . ": " . getPostLink($line) . "</li>";
                }
               echo "</ul>"; 
            }
        ?>
    </div>
</div>
</body>
</html>
