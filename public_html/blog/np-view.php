<?php
  $path_to_ini = $_SERVER['DOCUMENT_ROOT'] . "/../app/np.ini";
  $ini = parse_ini_file($path_to_ini);

  require_once($ini["app_folder"] . "/nerdpage.php");

  $blog_folder = "blog";
  $redirect = false;
  $post_nm = "";

  if(!isset($_GET["id"]) || empty($_GET["id"])) {
    $redirect = true;
  }
  else {

    $match = preg_match("/^\d{8}_\d{6}$/", $_GET["id"]);

    if( $match === false ){
      $redirect = true;
    }
    else { $post_nm = $_GET["id"]; }
  }

  if( $redirect ){
    header("Location: " . $ini["app_url"] . "/" . $blog_folder);
    die();
  }

  //Grab the filename from the csv and then get its contents
  $directory = 'posts';
  $post_id = -1;
  $filename = $post_date = $error = $title = "";
  $post_data = getPost($post_nm, $directory);

  if( $post_data !== FALSE && !empty($post_data) ) {
      $post_id = $post_data["id"];
      $post_date = $post_data["date"];
      $filename = $directory . "/" . $post_data["filename"];
      $title = $post_data["title"];
  }
  else {
    $error = "Bad ID";
  }

  //Get the file contents
  if( !empty($filename) && $error == "" ){
    $file = file_get_contents($filename);
    if( $file === false ) {
      $error = "Could not open file";
    }
    else { //increment view count
    
        $addOne = false;
        if(empty($_POST))
        {
            $addOne = true;
        }
        $cdir = $directory . "/comments";
        $hits = recordHit($post_id, $cdir, $addOne);
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/blog.css">
<link rel="stylesheet" href="/css/comments.css">
<title><?php echo $blog_folder; ?></title>
</head>
<body>
<div class="main">
    <h4 class="hits-count">views: <?php echo $hits; ?></h4>
    <h1><?php echo $title; ?></h1>
    <h4><?php echo formatLinuxDate($post_date); ?></h4>
    <?php
        echo $file;
    ?>
</div>
<?php include "np-comments.php"; ?>
</body>
</html> 
