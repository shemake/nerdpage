<?php
require_once($ini["app_folder"] . "/nerdpage.php");

$message = "";
$cdir = "posts/comments";
$max_subject_len = 50;
$max_author_len = 50;
$max_email_len = 320;
$max_comment_len = 3000;

if(isset($_POST['add_comment'])) { //check if form was submitted

    $passed_spam = true;
    //Check spam challenge question here as well
    if( isset($_POST['honeypot'] )  && ($_POST['honeypot'] !== "") )
    {
        $passed_spam = false;
    }

    if( isset($_POST['spamguard'] ) )
    {
        $data = array( "id" => $_POST['spamguard'], "answer" => $_POST['spam_answer']);
        $ok = checkSpamguard($data);
        if( !$ok )
        {
            $passed_spam = false;
        }
    }
    else
    {
        $passed_spam = false;
    }

    if( $passed_spam && !empty( $post_data ) )
    {
        $comment_data = array();
        $comment_data['subject'] = isset($_POST['subject']) ? $_POST['subject'] : "";
        $comment_data['author'] = isset($_POST['author']) ? $_POST['author'] : "";
        $comment_data['email'] = isset($_POST['email']) ? $_POST['email'] : "";
        $comment_data['text'] = isset($_POST['cdata']) ? $_POST['cdata'] : "";
        $comment_data['reply_to'] = isset($_POST['reply_to']) ? $_POST['reply_to'] : "";
        $comment_data['max_author_len'] = $max_author_len;
        $comment_data['max_subject_len'] = $max_subject_len;
        $comment_data['max_email_len'] = $max_email_len;
        $comment_data['max_comment_len'] = $max_comment_len;

        $message = submitComment( $comment_data, $post_data["id"], $cdir );
    }
    else
    {
        $message = "Sorry, comment could not be submitted.";
    }

    $_POST = array();
}
?>
<div class="comments-outer">
<div class="comments">
<h2>Comments</h2>
<?php
if( !empty($message) )
{
    echo "<span class=\"user-message\">" . $message . "</span>";
}

$spamguard = getSpamGuard();  
?>
<form action="" method="post" id="comment_form">
<div class="new-comment">
<table id="comment-table">
    <tr>
        <td><label for="name">Name:</label></td>
        <td><input type="text" id="author" name="author" maxlength="<?php echo $max_author_len; ?>"></td>
    </tr>
    <tr>
        <td><label for="email">Email:</label></td>
        <td><input type="text" id="email" name="email" maxlength="<?php echo $max_email_len; ?>"></td>
    </tr>
    <tr>
        <td colspan="2"><textarea name="cdata" maxlength="<?php echo $max_comment_len; ?>"></textarea></td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
<?php

if( !empty($spamguard) )
{
    echo "<tr>";
    echo "<td colspan=\"2\"><label for=\"spam_answer\">" . $spamguard['question'] . "</label></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td colspan=\"2\">";
    echo "<input type=\"text\" id=\"spam_answer\" name=\"spam_answer\" maxlength=\"" . $max_subject_len . "\">";
    echo "<input name=\"spamguard\" style=\"display: none\" tabindex=\"-1\" autocomplete=\"off\" value=\"" . $spamguard['id'] . "\">";
    echo "</td>";
    echo "</tr>";
}
?>
    <tr>
        <td></td>
        <td><input type="submit" name="add_comment" value="Leave Comment" /></td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: left; font-style: italic">Note: Email is optional. It will not be displayed, and is only for contact purposes.</td>
    </tr>
</table>
</div>
<input name="honeypot" style="display: none" tabindex="-1" autocomplete="off">
</form>
<?php
    //read in the comments for this post and display them here
    $postfiles = glob($cdir . "/p" . $post_data["id"] . "*.json");
    $grouped = groupComments($postfiles);
    foreach($grouped as $group)
    {
        $file = $group["file"];
        $contents = file_get_contents($file);
        if($contents)
        {
            $data = json_decode($contents, true);
            echo "<div class=\"comment\">";
            echo "<div class=\"comment-left\">";
            echo "<span class=\"comment-author\">" . $data['author'] . "</span>";
            echo "<span class=\"comment-date\">" . getDisplayDate($data['date']) . "</span>";
            echo "</div>";
            echo "<div class=\"comment-right\">";
            echo "<span class=\"comment-content\">" . nl2br($data['content']) . "</span>";
            echo "</div>";
            echo "</div>";
            echo "<div class=\"comment-reply\">" . getCommentReplyLink($data['id']) . "</div>";
        }

        foreach( $group["subs"] as $sub )
        {
            $contents = file_get_contents($sub);
            if( $contents )
            {
                $data = json_decode($contents, true);
                echo "<div class=\"comment-sub\">";
                echo "<div class=\"comment-left\">";
                echo "<span class=\"comment-author comment-sub-author\">" . $data['author'] . "</span>";
                echo "<span class=\"comment-date comment-sub-author\">" . getDisplayDate($data['date']) . "</span>";
                echo "</div>";
                echo "<div class=\"comment-right\">";
                echo "<span class=\"comment-content reply-color\">" . nl2br($data['content']) . "</span>";
                echo "</div>";
                echo "</div>";
            }
        }
    }
?>
</div>
</div>
