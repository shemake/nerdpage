<?php
  //A quick form to handle replying to comments
  $path_to_ini = $_SERVER['DOCUMENT_ROOT'] . "/../app/np.ini";
  $ini = parse_ini_file($path_to_ini);

  require_once($ini["app_folder"] . "/nerdpage.php");

  $blog_folder = "tech";
  $redirect = false;
  $comment_id = "";

  if(!isset($_GET["id"]) || empty($_GET["id"])) {
    $redirect = true;
  }
  else {

    $match = preg_match("/^p[0-9]+c[0-9]+$/", $_GET["id"]);

    //$letter_id = intval($_GET["id"]);
    if( $match === false ){
      $redirect = true;
    }
    else { $comment_id = $_GET["id"]; }
  }

  if( $redirect ){
    header("Location: " . $ini["app_url"] . "/" . $blog_folder);
    die();
  }

  $data = array();
  $cdir = "posts/comments";
  $message = "";
  $max_subject_len = 50;
  $max_author_len = 50;
  $max_email_len = 320;
  $max_comment_len = 3000;

  $filename = $cdir . "/" . $comment_id . ".json";
  if( !is_file($filename) )
  {
      $redirect = true;
  }
  else
  {
      $contents = file_get_contents($filename);
      if( $contents )
      {
          $data = json_decode($contents, true);
      }
      else
      {
          $redirect = true;
      }
  }

  if($redirect) {
      header("Location: " . $ini["app_url"] . "/" . $blog_folder);
      die();
  }

  $ids = parseCommentString($comment_id);
  $post_id = $ids['post_id']; 
  $post_data = getPostByID( $post_id );
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/blog.css">
<link rel="stylesheet" href="/css/comments.css">
<title><?php echo $blog_folder; ?></title>
</head>
<body>
<div class="comments-outer">
    <div class="comments">
<?php
     echo "<div class=\"comment\">";
     echo "<div class=\"comment-left\">";
     echo "<span class=\"comment-author\">" . $data['author'] . "</span>";
     echo "<span class=\"comment-date\">" . getDisplayDate($data['date']) . "</span>";
     echo "</div>";
     echo "<div class=\"comment-right\">";
     echo "<span class=\"comment-content\">" . nl2br($data['content']) . "</span>";
     echo "</div>";
     echo "</div>";
?>
<?php
     if(!empty($message) )
     {
         echo "<span class=\"user-message\">" . $message . "</span>";
     }
     
     $spamguard = getSpamGuard();  
?>
    <form action="<?php echo "../viewing/" . file_ext_strip($post_data["filename"]); ?>" method="post" id="comment_form">
    <div class="new-comment">
    <table id="comment-table">
        <tr>
            <td><label for="name">Name:</label></td>
            <td><input type="text" id="author" name="author" maxlength="<?php echo $max_author_len; ?>"></td>
        </tr>
        <tr>
            <td><label for="email">Email:</label></td>
            <td><input type="text" id="email" name="email" maxlength="<?php echo $max_email_len; ?>"></td>
        </tr>
        <tr>
            <td colspan="2"><textarea name="cdata" maxlength="<?php echo $max_comment_len; ?>"></textarea></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
         </tr>
<?php
if( !empty($spamguard) )
{
    echo "<tr>";
    echo "<td colspan=\"2\"><label for=\"spam_answer\">" . $spamguard['question'] . "</label></td>";
    echo "</tr>";
    echo "<tr>";
    echo "<td colspan=\"2\">";
    echo "<input type=\"text\" id=\"spam_answer\" name=\"spam_answer\" maxlength=\"" . $max_subject_len . "\">";
    echo "<input name=\"spamguard\" style=\"display: none\" tabindex=\"-1\" autocomplete=\"off\" value=\"" . $spamguard['id'] . "\">";
    echo "</td>";
    echo "</tr>";
}
?>
        <tr>
            <td></td>
            <td><input type="submit" name="add_comment" value="Reply" /></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left">Note: Email is optional. It will not be displayed, and is only for contact purposes.</td>
        </tr>
    </table>
    </div>
    <input name="honeypot" style="display: none" tabindex="-1" autocomplete="off">
    <input name="reply_to" style="display: none" tabindex="-1" autocomplete="off" value="<?php echo $comment_id; ?>">
    </form>
    </div>
</div>
</body>
</html> 
